    /* Get shit from another buffer associated with the same text view. */
    console = gtk_text_view_get_buffer(GTK_TEXT_BUFFER(readme));

    /* Write something into the new buffer. */
    gtk_text_buffer_set_text(console, "Hello, New buffer!\n", -1);

    while(gtk_events_pending()) {
        gtk_main_iteration();
        gtk_progress_bar_pulse(GTK_PROGRESS_BAR(progressbar));
    }


        gchar *message = g_strdup_printf("%.0f%% Complete", percent);

        gtk_progress_bar_set_text(progressbar, message);
        progress = g_strdup_printf("Progress at %.02f\n", percent);
        gtk_progress_bar_pulse(GTK_PROGRESS_BAR(progressbar));

        /* Provide feedback to user what is happening. */
        gtk_text_buffer_get_end_iter(console, &iterEnd);
        gtk_text_buffer_insert(console, &iterEnd, progress, -1);
        
