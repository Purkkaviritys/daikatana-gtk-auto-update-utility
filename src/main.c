#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>

GtkBuilder  *builder;
GtkWidget   *statusbar;
GtkWidget   *progressbar;
GtkWidget   *btn_exit;
GtkWidget   *btn_check_updates;
GtkTextView     *textview;
GtkTextBuffer   *console;

/* Is called when window is closed. */
void on_window_main_destroy()
{
    gtk_main_quit();
}

/* Is called when exit button is clicked. Do I need cleanup code? */
void on_btn_exit_clicked()
{
    gtk_main_quit();
}

/* Is called when user starts the update process. */
void dk_update_check_for_update(GtkWidget *widget, gpointer data)
{
    /* Glib character buffer for the statusbar message. */
    GtkTextIter iterStart, iterEnd;
    gchar       *statusMsg;
    gdouble     percent = 0.0;
    gchar       *array;

    /* Disable button while the update checking is on going. */
    gtk_widget_set_sensitive(btn_check_updates, FALSE);

    /* Set pulse as we don't know how long it will take to fetch a file. */
    gtk_progress_bar_set_pulse_step(GTK_PROGRESS_BAR(progressbar), 0.1);
    gtk_progress_bar_pulse(GTK_PROGRESS_BAR(progressbar));

    /* Set statusbar message. */
    statusMsg = g_strdup_printf("Checking for Update");
    gtk_statusbar_push(GTK_STATUSBAR(statusbar), GPOINTER_TO_INT(data), statusMsg);
    g_free(statusMsg);

    /* Set-up a console buffer. */
    console = gtk_text_buffer_new(NULL);

    /* Switch text buffer for the text view. */
    gtk_text_view_set_buffer(textview, console);

    /* Find the end of buffer. */
    gtk_text_buffer_get_end_iter(console, &iterEnd);
    gtk_text_buffer_insert(console, &iterEnd, "Started process.\n", -1);

    /* In a case of an error, send a message to screen and enable button
     * to check updates, don't clean up, output an update_error.log or something
     * like that for user. */
    // gtk_widget_set_sensitive(btn_check_updates, TRUE);

    /* If update process completes successfully, send a message to screen, don't
     * enable check update button, clean up. */

    while (percent <= 100.0) {
        gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(progressbar), percent / 100.0);


        /* Calculate the MD5s for PAKs. */
        // Daikatana_Update_Calculate_

        while (gtk_events_pending()) {
            gtk_main_iteration();
        }
        g_usleep(200000);
        percent += 2.0;
    }

    /* Print new log message into the buffer. */
    gtk_text_buffer_insert(console, &iterEnd, "Process complete.\n", -1);

    /* Throw in a new status message. */
    statusMsg = g_strdup_printf("Updating Completed");
    gtk_statusbar_push(GTK_STATUSBAR(statusbar), GPOINTER_TO_INT(data), statusMsg);
    g_free(statusMsg);

    /* Set button active again in case something goes wrong, like e.g. internet
     * connection is lost during download and then user wants to try again. */
    gtk_widget_set_sensitive(btn_check_updates, TRUE);

    /* Save the process to a file. */
    gtk_text_buffer_get_start_iter(console, &iterStart);
    gtk_text_buffer_get_end_iter(console, &iterEnd);
    array = gtk_text_buffer_get_text(console, &iterStart, &iterEnd, FALSE);
    g_file_set_contents("update.log", array, -1, NULL);
    g_free(array);
}

/* Print a status message to console. */
void print_hello()
{
    g_print("Hello\n");
}

int main(int argc, char *argv[])
{

    GtkWidget       *window;
    GtkWidget       *image;
    GtkWidget       *layout;
    GtkTextBuffer   *readme;
    GtkCssProvider  *cssprovider;
    gint            context_id;
    gchar           *array;
    gchar           *statusMsg;

    gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    cssprovider = gtk_css_provider_new();

    gtk_builder_add_from_file(builder, "glade/window_main.glade", NULL);
    gtk_css_provider_load_from_path(cssprovider, "resources/daikatana-auto-update.css", NULL);

    window = GTK_WIDGET(gtk_builder_get_object(builder, "dk_updater_main_window"));
    layout = GTK_WIDGET(gtk_builder_get_object(builder, "dk_updater_layout"));
    statusbar = GTK_WIDGET(gtk_builder_get_object(builder, "dk_updater_statusbar"));
    progressbar = GTK_WIDGET(gtk_builder_get_object(builder, "dk_updater_progressbar"));

    /* Get text widget and its buffers for switching between them easily. */
    console = GTK_TEXT_BUFFER(gtk_builder_get_object(builder, "dk_updater_textbuffer_console"));
    readme = GTK_TEXT_BUFFER(gtk_builder_get_object(builder, "dk_updater_textbuffer_readme"));
    textview = GTK_TEXT_VIEW(gtk_builder_get_object(builder, "dk_updater_textview"));

    /* Set style and context providers. */
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
            GTK_STYLE_PROVIDER(cssprovider), GTK_STYLE_PROVIDER_PRIORITY_USER);
    context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "Statusbar Message");

    /* Show 1.3 changelog.txt in a text field screen when starting the program
     * and once update process is triggered clear it up and show information
     * about the update progress. */

    /* Fetch latest changelog.txt from the internet? Talk to Maraakate about
     * possiblity of doing that. */


    /* Get a complete textfile into the gchar buffer. */
    g_file_get_contents("docs/changes.txt", &array, NULL, NULL);
    gtk_text_buffer_set_text(readme, array, -1);
    g_free(array);

    /* I have no idea how to make this scale properly. */
    image = gtk_image_new_from_file("resources/background.png");
    gtk_layout_put(GTK_LAYOUT(layout), image, 0, 0);

    /* Set statusbar message. */
    statusMsg = g_strdup_printf("Daikatana 1.3 Auto-Updater");
    gtk_statusbar_push(GTK_STATUSBAR(statusbar), GPOINTER_TO_INT(context_id), statusMsg);
    g_free(statusMsg);

    btn_check_updates = gtk_button_new_with_label("Check for Updates");
    
    gtk_layout_put(GTK_LAYOUT(layout), btn_check_updates, 256, 70);
    
    gtk_widget_set_size_request(btn_check_updates, 150, 40);

    g_signal_connect(G_OBJECT(btn_check_updates), "clicked",
                     G_CALLBACK(dk_update_check_for_update), GINT_TO_POINTER(context_id));


    btn_exit = gtk_button_new_with_label("Exit");
    gtk_layout_put(GTK_LAYOUT(layout), btn_exit, 256, 125);
    gtk_widget_set_size_request(btn_exit, 150, 40);
    g_signal_connect(G_OBJECT(btn_exit), "clicked", G_CALLBACK(on_btn_exit_clicked), NULL);


    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(builder);
    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
