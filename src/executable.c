#include <libelf.h>
#include <elf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int dk_test_executable()
{
	FILE *fp;
	Elf64_Ehdr *hdr;
	Elf *myelf;

	fp = fopen("./daikatana", "r+");

	if (fp == NULL) {
		printf("Problem opening Daikatana executable.\n" \
		       "Was updater run from same directory as the executable?\n");
		exit (1);
	}

	if (elf_version(EV_CURRENT) == EV_NONE) {
	}

	myelf = elf_begin(fp, ELF_C_READ, (Elf *)0);
	assert(myelf);
	hdr = elf64_getehdr(myelf);

	if (hdr != NULL) {
		switch (hdr->e_type) {
		case 1:
			cout << "File Type: ET_REL 1 /* Relocatable file */" << endl;
			break;
		case 2:
			cout << "File Type: ET_EXEC 2 /* Executable file */" << endl;
			break;
		default:
			cout << "File Type: Other type" << endl;
		}

		switch (hdr->e_machine) {
		case 0:
			cout << "Machine type: EM_NONE 0 /* No machine */" << endl;
			break;
		case 3:
			cout << "Machine type: EM_386 3 /* Intel 80386 */" << endl;
			break;
		case 62:
			cout << "Machine type: EM_X86_64 /* AMD x86-64 */" << endl;
			break;
		default:
			cout << hdr->e_machine << endl;
			cout << "Other Type" << endl;
		}
	} else {
		cout << "hdr ptr points to NULL" << endl;
	}

	return (0);
}
